﻿// <copyright file="GameElementModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using Newtonsoft.Json;

    /// <summary>
    /// Model for game elements.
    /// </summary>
    public class GameElementModel : IGameElementModel
    {
        private readonly double cachedX = default;
        private readonly double cachedY = default;
        private readonly double cachedDegree = default;

        /// <summary>
        /// GameModel current rotational degree.
        /// </summary>
        private double rotationalDegree;

        private Geometry cachedArea;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameElementModel"/> class.
        /// </summary>
        /// <param name="xPosition">GameModel x position.</param>
        /// <param name="yPosition">GameModel y position.</param>
        [JsonConstructor]
        public GameElementModel(double xPosition, double yPosition)
        {
            this.XPosition = xPosition;
            this.YPosition = yPosition;
        }

        /// <summary>
        /// Gets or sets game model covered area.
        /// </summary>
        public Geometry Area { get; set; }

        /// <inheritdoc/>
        public double XPosition { get; set; }

        /// <inheritdoc/>
        public double YPosition { get; set; }

        /// <summary>
        /// Gets or sets rotational degree in radian of GameModel.
        /// </summary>
        public double Radian
        {
            get
            {
                return Math.PI * this.rotationalDegree / 180;
            }

            set
            {
                this.rotationalDegree = 180 * value / Math.PI;
            }
        }

        /// <inheritdoc/>
        public Geometry RealArea
        {
            get
            {
                if (this.cachedArea == null || (this.cachedX != this.XPosition || this.cachedY != this.YPosition || this.cachedDegree != this.rotationalDegree))
                {
                    TransformGroup tg = new TransformGroup();
                    tg.Children.Add(new TranslateTransform(this.XPosition, this.YPosition));
                    tg.Children.Add(new RotateTransform(this.rotationalDegree, this.XPosition, this.YPosition));
                    this.Area.Transform = tg;
                    this.cachedArea = this.Area;
                    return this.Area.GetFlattenedPathGeometry();
                }
                else
                {
                    return this.cachedArea.GetFlattenedPathGeometry();
                }
            }
        }

        /// <inheritdoc/>
        public bool IsCollision(IGameElementModel other)
        {
            return Geometry.Combine(this.RealArea, other.RealArea, GeometryCombineMode.Intersect, null).GetArea() > 0;
        }
    }
}