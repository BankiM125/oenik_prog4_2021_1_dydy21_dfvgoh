﻿// <copyright file="IApproachingElementModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Approaching element model interface.
    /// </summary>
    public interface IApproachingElementModel : IGameElementModel
    {
        /// <summary>
        /// Gets or sets horizontal speed for moving the GameElement.
        /// </summary>
        double Speed { get; set; }

        /// <summary>
        /// Gets or sets name of GameElement.
        /// </summary>
        string Name { get; set; }
    }
}
