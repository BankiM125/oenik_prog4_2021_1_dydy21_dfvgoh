﻿// <copyright file="IGameModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Model
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Crossy.Model;
    using Crossy.Model.Interfaces;

    /// <summary>
    /// Game model interface.
    /// </summary>
    public interface IGameModel
    {
        /// <summary>
        /// Gets or sets the player.
        /// </summary>
        IPlayerModel Player { get; set; }

        /// <summary>
        /// Gets or sets a list of all enemies on screen.
        /// </summary>
        List<IEnemyModel> EnemiesOnScreen { get; set; }

        /// <summary>
        /// Gets or sets a list of all feed on screen.
        /// </summary>
        List<IGoldModel> GoldOnScreen { get; set; }

        /// <summary>
        /// Gets or sets a value storing the highscore.
        /// </summary>
        int Highscore { get; set; }

        /// <summary>
        /// Gets or sets a value storing the current score.
        /// </summary>
        int Score { get; set; }

        /// <summary>
        /// Gets or sets a value storing the current fish score.
        /// </summary>
        int GoldCollected { get; set; }

        /// <summary>
        /// Gets or sets a value storing how fast the game is.
        /// </summary>
        int Difficulty { get; set; }

        /// <summary>
        /// Gets value of MainWindow Width.
        /// </summary>
        double GameWidth { get; }

        /// <summary>
        /// Gets value of MainWindow Height.
        /// </summary>
        double GameHeight { get; }

        /// <summary>
        /// Gets or sets the upper border of the traversable map.
        /// </summary>
        double GameUpperBorder { get; set; }

        /// <summary>
        /// Gets or sets ingame level.
        /// </summary>
        int Level { get; set; }
    }
}
