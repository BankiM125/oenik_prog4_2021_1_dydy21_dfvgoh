﻿// <copyright file="GoldModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using Crossy.Model;
    using Newtonsoft.Json;

    /// <summary>
    /// Model class for gold.
    /// </summary>
    public class GoldModel : GameElementModel, IGoldModel
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GoldModel"/> class.
        /// </summary>
        /// <param name="xPosition">Coin x position.</param>
        /// <param name="yPosition">Coin y position.</param>
        /// <param name="value">Coin worth value.</param>
        /// <param name="hasrandomapproach">Coin approach value.</param>
        /// <param name="speed">Coin speed value.</param>
        [JsonConstructor]
        public GoldModel(double xPosition, double yPosition, int value, bool hasrandomapproach, double speed)
            : base(xPosition, yPosition)
        {
            this.Value = value;
            this.HasRandomApproach = hasrandomapproach;
            this.Speed = speed;

            if (value == 3)
            {
                this.Name = "expensive";
            }
            else
            {
                this.Name = "cheap";
            }

            RectangleGeometry placeholderarea = new RectangleGeometry(new Rect(xPosition, yPosition, 50, 50));
            this.Area = placeholderarea;
        }

        /// <inheritdoc/>
        public int Value { get; set; }

        /// <inheritdoc/>
        public bool HasRandomApproach { get; set; }

        /// <inheritdoc/>
        public double Speed { get; set; }

        /// <inheritdoc/>
        public string Name { get; set; }
    }
}
