﻿// <copyright file="LeaderboardEntryModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Crossy.Model.Interfaces;

    /// <summary>
    /// Leader board entry class.
    /// </summary>
    public class LeaderboardEntryModel : ILeaderboardEntryModel
    {
        /// <inheritdoc/>
        public string PlayerName { get; set; }

        /// <inheritdoc/>
        public int Score { get; set; }
    }
}
