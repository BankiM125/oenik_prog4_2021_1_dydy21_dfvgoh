﻿// <copyright file="EscapePage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp_CrossyToad
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using WpfAppCrossyToad;

    /// <summary>
    /// Interaction logic for EscapePage.xaml.
    /// </summary>
    public partial class EscapePage : Page
    {
        private GameControl control;

        /// <summary>
        /// Initializes a new instance of the <see cref="EscapePage"/> class.
        /// </summary>
        /// <param name="control">control from GamePage.</param>
        public EscapePage(GameControl control)
        {
            this.InitializeComponent();
            this.control = control;
            if (control is not null)
            {
                this.loadlist.ItemsSource = control.LoadMaps();
            }
        }

        private void Page_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)
            {
                this.NavigationService.GoBack();
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MainMenu());
        }

        private void Save_Click(object sender, RoutedEventArgs e)
        {
            this.control.SaveGame();
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            if (this.loadlist.SelectedItem != null)
            {
                this.control.LoadGame(this.loadlist.SelectedItem.ToString());
                this.NavigationService.GoBack();
            }
        }
    }
}
