﻿// <copyright file="MenuControl.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp_CrossyToad
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Threading;
    using Crossy.Logic;
    using Crossy.Logic.Interfaces;
    using Crossy.Model;
    using Crossy.Model.Interfaces;
    using Crossy.Renderer;

    /// <summary>
    /// Menu control.
    /// </summary>
    public class MenuControl
    {
        private IGameModel model;
        private IGameLogic gamelogic;
        private IMenuLogic menulogic;
        private GameRenderer gameRenderer;
       /* private DispatcherTimer timer;
        private DispatcherTimer maptimer;
        private bool isKeyDown = false;
        private bool isGameOver = false;*/

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuControl"/> class.
        /// </summary>
        public MenuControl()
        {
            this.model = new GameModel(0, 0, 0 /*this.model.Highscore*/, 1); // ide pname, highcore, difficulty a menüből/repóból

            this.gamelogic = new GameLogic(this.model);
            this.menulogic = new MenuLogic(this.model);
            this.gameRenderer = new GameRenderer(this.model);
        }

        /// <summary>
        /// Returns saved maps.
        /// </summary>
        /// <param name="mapname">saved map name.</param>
        public void LoadGame(string mapname)
        {
            this.gamelogic.LoadMap(mapname);
        }

        /// <summary>
        /// Returns saved maps.
        /// </summary>
        /// <returns>List of maps.</returns>
        public List<string> LoadMaps()
        {
            return this.gamelogic.ListSavedMaps();
        }

        /// <summary>
        /// Sets player name.
        /// </summary>
        /// <param name="playername">Name of player.</param>
        public void SetName(string playername)
        {
            this.menulogic.SetPlayerName(playername);
        }

        /// <summary>
        /// Gets current fish count.
        /// </summary>
        /// <returns>Number of collected fish.</returns>
        public int GetCollectedGold()
        {
           return this.menulogic.GetCollectedGold();
        }

        /// <summary>
        /// Gets all leaderboard entries.
        /// </summary>
        /// <returns>Leaderboards entries.</returns>
        public List<ILeaderboardEntryModel> GetLeaderboards()
        {
            return this.menulogic.DisplayHighscore();
        }
    }
}
