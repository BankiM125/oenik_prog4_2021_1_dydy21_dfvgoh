﻿// <copyright file="StartGamePage.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace WpfApp_CrossyToad
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Navigation;
    using System.Windows.Shapes;
    using WpfAppCrossyToad;

    /// <summary>
    /// Interaction logic for StartGamePage.xaml.
    /// </summary>
    /// <summary>
    /// Interaction logic for PlayGamePage.
    /// </summary>
    public partial class StartGamePage : Page
    {
        private MenuControl control;

        /// <summary>
        /// Initializes a new instance of the <see cref="StartGamePage"/> class.
        /// </summary>
        public StartGamePage()
        {
            this.InitializeComponent();
            this.control = new MenuControl();
            this.loadlist.ItemsSource = this.control.LoadMaps();
        }

        private void Menu_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.GoBack();
        }

        private void New_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new GamePage());
        }

        private void Load_Click(object sender, RoutedEventArgs e)
        {
            if (this.loadlist.SelectedItem != null)
            {
                this.NavigationService.Navigate(new GamePage(this.loadlist.SelectedItem.ToString()));
            }
        }

        private void SetName_Click(object sender, RoutedEventArgs e)
        {
            this.control.SetName(this.textbox.Text);
        }
    }
}
