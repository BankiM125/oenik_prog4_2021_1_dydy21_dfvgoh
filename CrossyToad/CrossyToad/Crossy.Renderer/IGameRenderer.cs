﻿// <copyright file="IGameRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Renderer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;

    /// <summary>
    /// Interface for the game renderer.
    /// </summary>
    public interface IGameRenderer
    {
        /// <summary>
        /// Builds the drawing.
        /// </summary>
        /// <returns>A drawing.</returns>
        Drawing BuildDrawing();

        /// <summary>
        /// Game over.
        /// </summary>
        /// <returns>A drwaing.</returns>
        Drawing GameOver();

        /// <summary>
        /// Reset.
        /// </summary>
        void Reset();
    }
}
