﻿// <copyright file="GameBrushes.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Renderer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;

    /// <summary>
    /// Class for the game brushes.
    /// </summary>
    public class GameBrushes
    {
        private string level;
        private Dictionary<string, Brush> brushes = new Dictionary<string, Brush>();

        /// <summary>
        /// Gets or sets game level.
        /// </summary>
        public string Level
        {
            get { return this.level; }
            set { this.level = value; }
        }

        /// <summary>
        /// Gets Player brush.
        /// </summary>
        public Brush PlayerBrush
        {
            get { return this.GetBrush($"WpfApp_CrossyToad.Images.player"); }
        }

        /// <summary>
        /// Gets cheap  Gold brush.
        /// </summary>
        public Brush CheapGoldBrush
        {
            get { return this.GetBrush($"WpfApp_CrossyToad.Images.gold"); }
        }

        /// <summary>
        /// Gets Background brush.
        /// </summary>
        public Brush BackgroundBrush
        {
            get { return this.GetBrush($"WpfApp_CrossyToad.Images.background"); }
        }

        /// <summary>
        /// Gets Enemy brush.
        /// </summary>
        public Brush WeakEnemyBrush
        {
            get { return this.GetBrush($"WpfApp_CrossyToad.Images.ufo"); }
        }

        /// <summary>
        /// Gets Enemy brush.
        /// </summary>
        public Brush StrongEnemyBrush
        {
            get { return this.GetBrush($"WpfApp_CrossyToad.Images.strong_ufo"); }
        }

        /// <summary>
        /// Gets expensive gold brush.
        /// </summary>
        public Brush ExpensiveGoldBrush
        {
            get { return this.GetBrush($"WpfApp_CrossyToad.Images.expensive_gold"); }
        }

        /// <summary>
        /// Gets Heart brush.
        /// </summary>
        public Brush HeartBrush
        {
            get { return this.GetBrush($"WpfApp_CrossyToad.Images.heart"); }
        }

        private Brush GetBrush(string filename)
        {
            if (!this.brushes.ContainsKey(filename + this.level))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.StreamSource = Assembly.GetEntryAssembly().GetManifestResourceStream(filename + this.level + ".png");
                bmp.EndInit();
                ImageBrush ib = new ImageBrush(bmp);

                this.brushes.Add(filename + this.level, ib);
            }

            return this.brushes[filename + this.level];
        }
    }
}
