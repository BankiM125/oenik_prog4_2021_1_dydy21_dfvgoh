﻿// <copyright file="GameRenderer.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Renderer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Media;
    using Crossy.Model;

    /// <summary>
    /// Renderer class for the game.
    /// </summary>
    public class GameRenderer : IGameRenderer
    {
        private readonly IGameModel model;
        private Drawing oldBackground;
        private Brush oldPlayerBrush;
        private Brush oldStrongEnemyBrush;
        private Brush oldHeartBrush;
        private Brush oldExpensiveGoldBrush;
        private Brush oldCheapGoldBrush;
        private Brush oldWeakEnemyBrush;
        private int oldscore;
        private int oldlives;
        private string oldlevel;
        private GameBrushes gameBrushes;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameRenderer"/> class.
        /// </summary>
        /// <param name="model">Game model interface.</param>
        public GameRenderer(IGameModel model)
        {
            this.model = model;
            this.gameBrushes = new GameBrushes();
            this.gameBrushes.Level = this.model.Level.ToString(System.Globalization.CultureInfo.CurrentCulture);
            this.oldscore = this.model.GoldCollected;
            this.oldlives = this.model.Player.LivesTotal;
            this.oldPlayerBrush = this.gameBrushes.PlayerBrush;
            this.oldStrongEnemyBrush = this.gameBrushes.StrongEnemyBrush;
            this.oldWeakEnemyBrush = this.gameBrushes.WeakEnemyBrush;
            this.oldExpensiveGoldBrush = this.gameBrushes.ExpensiveGoldBrush;
            this.oldCheapGoldBrush = this.gameBrushes.CheapGoldBrush;
            this.oldHeartBrush = this.gameBrushes.HeartBrush;
        }

        /// <inheritdoc/>
        public Drawing BuildDrawing()
        {
            DrawingGroup dg = new DrawingGroup();
            this.gameBrushes.Level = this.model.Level.ToString(System.Globalization.CultureInfo.CurrentCulture);
            dg.Children.Add(this.GetBackground());

            foreach (Drawing enemy in this.GetEnemies())
            {
                dg.Children.Add(enemy);
            }

            foreach (Drawing gold in this.GetGold())
            {
                dg.Children.Add(gold);
            }

            dg.Children.Add(this.GetPlayer());

            foreach (Drawing item in this.GetOverlay())
            {
                dg.Children.Add(item);
            }

            return dg;
        }

        /// <summary>
        /// Resets all cached values.
        /// </summary>
        public void Reset()
        {
            this.oldBackground = null;
            this.oldPlayerBrush = null;
            this.oldlevel = null;
            this.oldCheapGoldBrush = null;
            this.oldExpensiveGoldBrush = null;
            this.oldHeartBrush = null;
            this.oldscore = 0;
            this.oldlives = 0;
        }

        /// <summary>
        /// Draws game over text.
        /// </summary>
        /// <returns>text.</returns>
        public Drawing GameOver()
        {
            FormattedText gameovertext = new FormattedText(
                "Game Over",
                System.Globalization.CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface("Rockwell"),
                170,
                Brushes.Black,
                1.0);
            return new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, 0.5), gameovertext.BuildGeometry(new Point(this.model.GameWidth / 5, this.model.GameHeight / 4)));
        }

        private List<Drawing> GetGold()
        {
            List<Drawing> goldmodels = new List<Drawing>();
            foreach (var item in this.model.GoldOnScreen)
            {
                if (item.Name == "expensive" && this.gameBrushes.Level != this.oldlevel)
                {
                    this.oldExpensiveGoldBrush = this.gameBrushes.ExpensiveGoldBrush;
                    goldmodels.Add(new GeometryDrawing(this.oldExpensiveGoldBrush, null, item.RealArea));
                }
                else if (item.Name == "expensive")
                {
                    goldmodels.Add(new GeometryDrawing(this.oldExpensiveGoldBrush, null, item.RealArea));
                }

                if (item.Name == "cheap" && this.gameBrushes.Level != this.oldlevel)
                {
                    this.oldCheapGoldBrush = this.gameBrushes.CheapGoldBrush;
                    goldmodels.Add(new GeometryDrawing(this.oldCheapGoldBrush, null, item.RealArea));
                }
                else if (item.Name == "cheap")
                {
                    goldmodels.Add(new GeometryDrawing(this.oldCheapGoldBrush, null, item.RealArea));
                }
            }

            return goldmodels;
        }

        private List<Drawing> GetOverlay()
        {
            List<Drawing> overlays = new List<Drawing>();

            this.oldlives = this.model.Player.LivesLeft;
            for (int i = 0; i < this.oldlives; i++)
            {
                if (i < 3)
                {
                    overlays.Add(new GeometryDrawing(this.oldHeartBrush, null, new RectangleGeometry(new Rect(i * 60, 0, 50, 50))));
                }
                else
                {
                    overlays.Add(new GeometryDrawing(this.oldHeartBrush, null, new RectangleGeometry(new Rect(i * 60, 0, 50, 50))));
                }
            }

            FormattedText scoretext = new FormattedText(
                this.model.Score.ToString(System.Globalization.CultureInfo.CurrentCulture) + "(" + this.model.Highscore + ")",
                System.Globalization.CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface("Rockwell"),
                40,
                Brushes.Black,
                1.0);

            overlays.Add(new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, 0.5), scoretext.BuildGeometry(new Point(0, 55))));

            overlays.Add(new GeometryDrawing(this.oldCheapGoldBrush, null, new RectangleGeometry(new Rect(0, 110, 40, 40))));
            if (this.oldscore != this.model.GoldCollected)
            {
                FormattedText fishtext = new FormattedText(
                    this.model.GoldCollected.ToString(System.Globalization.CultureInfo.CurrentCulture),
                    System.Globalization.CultureInfo.CurrentCulture,
                    FlowDirection.LeftToRight,
                    new Typeface("Rockwell"),
                    40,
                    Brushes.Black,
                    1.0);
                overlays.Add(new GeometryDrawing(Brushes.Black, new Pen(Brushes.Black, 0.5), fishtext.BuildGeometry(new Point(40, 105))));
            }

            return overlays;
        }

        private List<Drawing> GetEnemies()
        {
            List<Drawing> enemymodels = new List<Drawing>();
            foreach (var item in this.model.EnemiesOnScreen)
            {
                if (item.Name == "strong" && this.gameBrushes.Level != this.oldlevel)
                {
                    this.oldStrongEnemyBrush = this.gameBrushes.StrongEnemyBrush;
                    enemymodels.Add(new GeometryDrawing(this.oldStrongEnemyBrush, null, item.RealArea));
                }
                else if (item.Name == "strong")
                {
                    enemymodels.Add(new GeometryDrawing(this.oldStrongEnemyBrush, null, item.RealArea));
                }

                if (item.Name == "weak" && this.gameBrushes.Level != this.oldlevel)
                {
                    this.oldWeakEnemyBrush = this.gameBrushes.WeakEnemyBrush;
                    enemymodels.Add(new GeometryDrawing(this.oldWeakEnemyBrush, null, item.RealArea));
                }
                else if (item.Name == "weak")
                {
                    enemymodels.Add(new GeometryDrawing(this.oldWeakEnemyBrush, null, item.RealArea));
                }
            }

            return enemymodels;
        }

        private Drawing GetBackground()
        {
            if (this.oldBackground == null || this.gameBrushes.Level != this.oldlevel)
            {
                Geometry g = new RectangleGeometry(new Rect(0, 0, this.model.GameWidth, this.model.GameHeight));
                this.oldBackground = new GeometryDrawing(this.gameBrushes.BackgroundBrush, null, g);
            }

            return this.oldBackground;
        }

        private Drawing GetPlayer()
        {
            if (this.gameBrushes.Level != this.oldlevel)
            {
                this.oldPlayerBrush = this.gameBrushes.PlayerBrush;
                return new GeometryDrawing(this.oldPlayerBrush, null, this.model.Player.RealArea);
            }
            else
            {
                return new GeometryDrawing(this.oldPlayerBrush, null, this.model.Player.RealArea);
            }
        }
    }
}
