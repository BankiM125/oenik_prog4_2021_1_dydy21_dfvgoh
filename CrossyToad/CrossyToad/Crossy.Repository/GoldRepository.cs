﻿// <copyright file="GoldRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Repository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Repository for the gold entity.
    /// </summary>
    public class GoldRepository
    {
        /// <summary>
        /// File containing number of collected fish.
        /// </summary>
        private readonly string goldRepoPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
            + "\\CrossyToad\\CollectedGold\\";

        /// <summary>
        /// Initializes a new instance of the <see cref="GoldRepository"/> class.
        /// </summary>
        public GoldRepository()
        {
            Directory.CreateDirectory(this.goldRepoPath);
        }

        /// <summary>
        /// Increases number of fish stored.
        /// </summary>
        /// <param name="gold">Number of fish to add.</param>
        public virtual void Add(int gold)
        {
            int current = gold + this.Read();

            StreamWriter sw = new StreamWriter(this.goldRepoPath + "gold.txt");
            sw.WriteLine(current.ToString(System.Globalization.CultureInfo.CurrentCulture));
            sw.Close();
        }

        /// <summary>
        /// Get current number of fish collected.
        /// </summary>
        /// <returns>The number of fish.</returns>
        public virtual int Read()
        {
            if (File.Exists(this.goldRepoPath + "gold.txt"))
            {
                StreamReader sr = new StreamReader(this.goldRepoPath + "gold.txt");
                string value = sr.ReadToEnd();
                sr.Close();
                if (string.IsNullOrEmpty(value))
                {
                    return 0;
                }

                return int.Parse(value, System.Globalization.CultureInfo.CurrentCulture);
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Decreases number of fish stored.
        /// </summary>
        /// <param name="gold">Number of fish to remove.</param>
        public void Remove(int gold)
        {
            if (File.Exists(this.goldRepoPath + "gold.txt"))
            {
                int currentgold = this.Read();
                StreamWriter sw = new StreamWriter(this.goldRepoPath + "gold.txt");
                sw.WriteLine((currentgold - gold).ToString(System.Globalization.CultureInfo.CurrentCulture));
                sw.Close();
            }
        }
    }
}
