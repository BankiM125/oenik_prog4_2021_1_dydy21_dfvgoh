﻿// <copyright file="MapRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Repository
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Web;
    using Crossy.Model;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// Repository for the map.
    /// </summary>
    public class MapRepository : IRepository<IGameModel>
    {
        /// <summary>
        /// Files containing saved maps.
        /// </summary>
        private readonly string mapRepoPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData)
            + "\\CrossyToad\\Maps\\";

        /// <summary>
        /// Initializes a new instance of the <see cref="MapRepository"/> class.
        /// </summary>
        public MapRepository()
        {
            Directory.CreateDirectory(this.mapRepoPath);
        }

        /// <summary>
        /// Gets the path of the repo directory.
        /// </summary>
        public string MapRepoPath => this.mapRepoPath;

        /// <inheritdoc/>
        public void Create(IGameModel data, string name)
        {
            StreamWriter sw = new StreamWriter(this.MapRepoPath + name + ".json");
            List<object> enemiesOnScreen = new List<object>();
            List<object> goldOnScreen = new List<object>();

            if (data is not null)
            {
                foreach (var item in data.EnemiesOnScreen)
                {
                    enemiesOnScreen.Add(
                        new
                        {
                            item.XPosition,
                            item.YPosition,
                            item.Damage,
                            item.HasAimBot,
                            item.Speed,
                        });
                }

                foreach (var item in data.GoldOnScreen)
                {
                    goldOnScreen.Add(
                        new
                        {
                            item.XPosition,
                            item.YPosition,
                            item.Value,
                            item.HasRandomApproach,
                            item.Speed,
                        });
                }

                var save = new
                {
                    Player = new
                    {
                        data.Player.XPosition,
                        data.Player.YPosition,
                        data.Player.LivesLeft,
                        data.Player.LivesTotal,
                    },
                    EnemiesOnScreen = enemiesOnScreen,
                    GoldOnScreen = goldOnScreen,
                    data.GameHeight,
                    data.GameWidth,
                    PlayerModel.PlayerName,
                    data.Highscore,
                    data.Score,
                    data.Difficulty,
                    data.GoldCollected,
                    data.Level,
                };

                sw.WriteLine(JsonConvert.SerializeObject(save));
            }

            sw.Close();
        }

        /// <inheritdoc/>
        public void Delete(IGameModel data)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public IGameModel Read(string name)
        {
            StreamReader sw = new StreamReader(this.MapRepoPath + name + ".json");
            JObject save = JObject.Parse(sw.ReadToEnd());

            // Get player
            JToken playerToken = save["Player"];

            // Get enemies
            IList<JToken> enemyTokens = save["EnemiesOnScreen"].Children().ToList();
            List<IEnemyModel> enemies = new List<IEnemyModel>();
            foreach (var item in enemyTokens)
            {
                enemies.Add(item.ToObject<EnemyModel>());
            }

            // Get food
            IList<JToken> goldTokens = save["GoldOnScreen"].Children().ToList();
            List<IGoldModel> gold = new List<IGoldModel>();
            foreach (var item in goldTokens)
            {
                gold.Add(item.ToObject<GoldModel>());
            }

            sw.Close();

            return new GameModel(
                double.Parse(save["GameHeight"].Value<string>(), System.Globalization.CultureInfo.CurrentCulture),
                double.Parse(save["GameWidth"].Value<string>(), System.Globalization.CultureInfo.CurrentCulture),
                int.Parse(save["Highscore"].Value<string>(), System.Globalization.CultureInfo.CurrentCulture),
                int.Parse(save["Difficulty"].Value<string>(), System.Globalization.CultureInfo.CurrentCulture))
            {
                EnemiesOnScreen = enemies,
                GoldOnScreen = gold,
                GoldCollected = int.Parse(save["GoldCollected"].Value<string>(), System.Globalization.CultureInfo.CurrentCulture),
                Level = int.Parse(save["Level"].Value<string>(), System.Globalization.CultureInfo.CurrentCulture),
                Player = playerToken.ToObject<PlayerModel>(),
                Score = int.Parse(save["Score"].Value<string>(), System.Globalization.CultureInfo.CurrentCulture),
            };
        }

        /// <inheritdoc/>
        public void Update(IGameModel existing, IGameModel newdata)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Get names of all saved maps.
        /// </summary>
        /// <returns>A list containing all saved maps.</returns>
        public List<string> ListSaves()
        {
            return Directory.GetFiles(this.MapRepoPath, "*.json")
                .Select(Path.GetFileNameWithoutExtension)
                .ToList();
        }
    }
}
