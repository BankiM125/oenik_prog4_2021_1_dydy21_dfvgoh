﻿// <copyright file="IPlayerLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Crossy.Model;

    /// <summary>
    /// Describes methods which are applicable to all elements that are approaching from the right.
    /// </summary>
    public interface IPlayerLogic : IGameElementLogic
    {
        /// <summary>
        /// Gets or sets current descent speed.
        /// </summary>
        double DescentSpeed { get; set; }

        /// <summary>
        /// Descend with simulated gravitational force.
        /// </summary>
        void Descend();

        /// <summary>
        /// Ascend steadily.
        /// </summary>
        void Ascend();

        /// <summary>
        /// Descrease lives.
        /// </summary>
        /// <param name="value">Delta of decrement.</param>
        void DecreaseLivesLeft(int value);
    }
}
