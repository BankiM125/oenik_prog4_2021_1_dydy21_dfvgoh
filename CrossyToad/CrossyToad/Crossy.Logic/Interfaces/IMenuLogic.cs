﻿// <copyright file="IMenuLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Crossy.Model;
    using Crossy.Model.Interfaces;

    /// <summary>
    /// Interface for the menu logic.
    /// </summary>
    public interface IMenuLogic
    {
        /// <summary>
        /// Gets or sets the model to manipulate.
        /// </summary>
        IGameModel GameModel { get; set; }

        /// <summary>
        /// Returns data from the repository representing the leaderboard.
        /// </summary>
        /// <returns>The list containing leaderboard entries.</returns>
        List<ILeaderboardEntryModel> DisplayHighscore();

        /// <summary>
        /// Sets player name in repository.
        /// </summary>
        /// <param name="playername">Player name.</param>
        void SetPlayerName(string playername);

        /// <summary>
        /// Gets player name in repository.
        /// </summary>
        /// <returns>Player name.</returns>
        string GetPlayerName();

        /// <summary>
        /// Get how much gold do we have.
        /// </summary>
        /// <returns>The amount of fish.</returns>
        int GetCollectedGold();
    }
}
