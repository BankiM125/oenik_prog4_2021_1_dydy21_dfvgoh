﻿// <copyright file="IEnemyLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Logic.Interfaces
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Describes methods which are applicable to Enemies.
    /// </summary>
    public interface IEnemyLogic : IApproachingElementLogic
    {
        /// <summary>
        /// Enter the game area on the right and aim for the player object.
        /// </summary>
        /// <param name="target">The target's place.</param>
        void AimBotApproach(double target);
    }
}
