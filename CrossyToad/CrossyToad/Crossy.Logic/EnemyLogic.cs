﻿// <copyright file="EnemyLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Crossy.Logic.Interfaces;
    using Crossy.Model;

    /// <summary>
    /// Logic for what enemies can do.
    /// </summary>
    public class EnemyLogic : ApproachingElementLogic, IEnemyLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="EnemyLogic"/> class.
        /// </summary>
        /// <param name="approachingElement">The element to be manipulated.</param>
        public EnemyLogic(IApproachingElementModel approachingElement)
            : base(approachingElement)
        {
        }

        /// <summary>
        /// Gets or sets the enemy to manipulate.
        /// </summary>
        public IEnemyModel Enemy
        {
            get
            {
                return (IEnemyModel)this.GameElement;
            }

            set
            {
            }
        }

        /// <inheritdoc/>
        public void AimBotApproach(double target)
        {
            this.Move(Direction.Left, this.ApproachingElement.Speed);
            if (this.Enemy.RealArea.Bounds.Bottom > target)
            {
                this.Move(Direction.Up, this.ApproachingElement.Speed / 3);
            }

            if (this.Enemy.RealArea.Bounds.Top < target)
            {
                this.Move(Direction.Down, this.ApproachingElement.Speed / 3);
            }
        }
    }
}
