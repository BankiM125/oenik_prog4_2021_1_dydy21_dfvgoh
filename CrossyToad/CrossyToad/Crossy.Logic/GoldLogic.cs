﻿// <copyright file="GoldLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Crossy.Logic.Interfaces;
    using Crossy.Model;

    /// <summary>
    /// Logic describing what gold can do.
    /// </summary>
    public class GoldLogic : ApproachingElementLogic, IGoldLogic
    {
        /// <summary>
        /// This counter is used in RandomApproach() to determine whether a direction change is necessary.
        /// </summary>
        private int tickCounter;

        /// <summary>
        /// Store the current state for RandomApproach().
        /// </summary>
        private int dice = 1;

        /// <summary>
        /// Initializes a new instance of the <see cref="GoldLogic"/> class.
        /// </summary>
        /// <param name="approachingElement">The element to be manipulated.</param>
        public GoldLogic(IApproachingElementModel approachingElement)
            : base(approachingElement)
        {
        }

        /// <summary>
        /// Gets or sets the food to manipulate.
        /// </summary>
        public IGoldModel Food
        {
            get
            {
                return (IGoldModel)this.GameElement;
            }

            set
            {
            }
        }

        /// <inheritdoc/>
        public void RandomApproach()
        {
            if (this.tickCounter % 10 == 0)
            {
                this.dice = -this.dice;
            }

            switch (this.dice)
            {
                case 1:
                    this.Move(Direction.Up, this.ApproachingElement.Speed);
                    break;
                case -1:
                    this.Move(Direction.Down, this.ApproachingElement.Speed);
                    break;
                default:
                    break;
            }

            this.Move(Direction.Left, this.ApproachingElement.Speed);

            this.tickCounter++;
        }
    }
}
