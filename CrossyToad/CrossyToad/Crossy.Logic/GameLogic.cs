﻿// <copyright file="GameLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Crossy.Logic.Interfaces;
    using Crossy.Model;
    using Crossy.Repository;

    /// <summary>
    /// Game Logic class.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        /// <summary>
        /// Just a random number generator.
        /// </summary>
        private readonly Random rng = new Random();

        private readonly MapRepository mapRepository;

        private readonly GoldRepository goldRepository;

        private readonly LeaderboardRepository leaderboardRepository;

        private int scoreCounter;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="gameModel">The model to manipulate.</param>
        public GameLogic(IGameModel gameModel)
        {
            this.GameModel = gameModel ?? throw new ArgumentNullException(nameof(gameModel));

            this.PlayerModelLogicPair = default;
            this.EnemyModelLogicPairs = new Dictionary<IEnemyModel, IEnemyLogic>();
            this.GoldModelLogicPairs = new Dictionary<IGoldModel, IGoldLogic>();

            this.mapRepository = new MapRepository();
            this.goldRepository = new GoldRepository();
            this.leaderboardRepository = new LeaderboardRepository();

            this.LoadHighscore();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="gameModel">The model.</param>
        /// <param name="mapRepository">The map repository.</param>
        /// <param name="leaderboardRepository">The leaderboard repo.</param>
        /// <param name="fishRepository">The fish repo.</param>
        public GameLogic(
            IGameModel gameModel,
            MapRepository mapRepository,
            LeaderboardRepository leaderboardRepository,
            GoldRepository fishRepository)
        {
            this.GameModel = gameModel ?? throw new ArgumentNullException(nameof(gameModel));

            this.PlayerModelLogicPair = default;
            this.EnemyModelLogicPairs = new Dictionary<IEnemyModel, IEnemyLogic>();
            this.GoldModelLogicPairs = new Dictionary<IGoldModel, IGoldLogic>();

            this.mapRepository = mapRepository;
            this.goldRepository = fishRepository;
            this.leaderboardRepository = leaderboardRepository;
        }

        /// <inheritdoc/>
        public event EventHandler GameOver;

        /// <inheritdoc/>
        public IGameModel GameModel { get; set; }

        /// <inheritdoc/>
        public KeyValuePair<IPlayerModel, IPlayerLogic> PlayerModelLogicPair { get; set; }

        /// <inheritdoc/>
        public Dictionary<IEnemyModel, IEnemyLogic> EnemyModelLogicPairs { get; set; }

        /// <inheritdoc/>
        public Dictionary<IGoldModel, IGoldLogic> GoldModelLogicPairs { get; set; }

        /// <inheritdoc/>
        public void ApproachAll()
        {
            var toRemoveEnemies = new List<IEnemyModel>();
            var toRemoveFood = new List<IGoldModel>();

            // Move enemies
            foreach (var enemy in this.EnemyModelLogicPairs)
            {
                if (enemy.Key.HasAimBot)
                {
                    enemy.Value.AimBotApproach(this.PlayerModelLogicPair.Key.YPosition);
                }
                else
                {
                    enemy.Value.HorizontalApproach();
                }

                // Collision check with player
                if (this.PlayerModelLogicPair.Key.IsCollision(enemy.Key))
                {
                    this.PlayerModelLogicPair.Value.DecreaseLivesLeft(enemy.Key.Damage);
                    toRemoveEnemies.Add(enemy.Key);
                    if (this.PlayerModelLogicPair.Key.LivesLeft <= 0)
                    {
                        this.GameOver?.Invoke(this, EventArgs.Empty);
                    }
                }
            }

            // Move food
            foreach (var food in this.GoldModelLogicPairs)
            {
                if (food.Key.HasRandomApproach)
                {
                    food.Value.RandomApproach();
                }
                else
                {
                    food.Value.HorizontalApproach();
                }

                // Collision check with player
                if (this.PlayerModelLogicPair.Key.IsCollision(food.Key))
                {
                    int multiply = 1;

                    this.IncreaseGoldCollected(food.Key.Value * multiply);
                    toRemoveFood.Add(food.Key);
                }
            }

            // Run cleanup
            foreach (var item in toRemoveEnemies)
            {
                this.GameModel.EnemiesOnScreen.Remove(item);
                this.EnemyModelLogicPairs.Remove(item);
            }

            foreach (var item in toRemoveFood)
            {
                this.GameModel.GoldOnScreen.Remove(item);
                this.GoldModelLogicPairs.Remove(item);
            }

            // Add to score
            this.scoreCounter++;
            if (this.scoreCounter % 5 == 0)
            {
                this.GameModel.Score++;
            }

            // Check and set level
            if (this.GameModel.Score < 200)
            {
                this.GameModel.Level = 1;
            }
            else
            {
                this.GameModel.Level = 2;
            }
        }

        /// <inheritdoc/>
        public void AscendPlayer()
        {
            if (this.PlayerModelLogicPair.Key.RealArea.Bounds.Top >= 150)
            {
                this.PlayerModelLogicPair.Value.Ascend();
            }
        }

        /// <inheritdoc/>
        public void Cleanup()
        {
            for (int i = 0; i < this.EnemyModelLogicPairs.Count; i++)
            {
                var item = this.EnemyModelLogicPairs.ElementAt(i);
                if (item.Key.XPosition < 0 - (this.GameModel.GameWidth * 2))
                {
                    this.EnemyModelLogicPairs.Remove(item.Key);
                    this.GameModel.EnemiesOnScreen.Remove(item.Key);
                }
            }

            for (int i = 0; i < this.GoldModelLogicPairs.Count; i++)
            {
                var item = this.GoldModelLogicPairs.ElementAt(i);
                if (item.Key.XPosition < 0 - (this.GameModel.GameWidth * 2))
                {
                    this.GoldModelLogicPairs.Remove(item.Key);
                    this.GameModel.GoldOnScreen.Remove(item.Key);
                }
            }
        }

        /// <inheritdoc/>
        public void DescendPlayer()
        {
            if (this.PlayerModelLogicPair.Key.YPosition +
                this.PlayerModelLogicPair.Value.DescentSpeed >
                this.GameModel.GameHeight -
                this.GameModel.Player.RealArea.Bounds.Height)
            {
                this.PlayerModelLogicPair.Value.SetY(
                    this.GameModel.GameHeight -
                    this.GameModel.Player.RealArea.Bounds.Height);
            }

            if (this.PlayerModelLogicPair.Key.RealArea.Bounds.Bottom <
                this.GameModel.GameHeight)
            {
                this.PlayerModelLogicPair.Value.Descend();
            }
        }

        /// <inheritdoc/>
        public void GenerateMap()
        {
            // Enemies
            int enemiesToAddCount = 10 - this.GameModel.EnemiesOnScreen.Count;
            for (int i = 0; i < enemiesToAddCount; i++)
            {
                double xpos = (double)this.rng.Next(
                    (int)this.GameModel.GameWidth,
                    (int)this.GameModel.GameWidth * 2);
                double ypos = (double)this.rng.Next(150, (int)this.GameModel.GameHeight - 500);
                int dmg;
                bool aimbot;
                if (this.rng.Next(0, 10) < 8)
                {
                    dmg = 1;
                    aimbot = false;
                }
                else
                {
                    dmg = 3;
                    aimbot = true;
                }

                double speed = 10;
                EnemyModel enemyToAdd = new EnemyModel(xpos, ypos, dmg, aimbot, speed);
                bool addIsOk = true;
                foreach (var item in this.GameModel.EnemiesOnScreen)
                {
                    if (enemyToAdd.IsCollision(item) ||
                        (Math.Abs(enemyToAdd.YPosition - item.YPosition) < 100 &&
                        Math.Abs(enemyToAdd.XPosition - item.XPosition) < 200) ||
                        (item.Damage == 3 && enemyToAdd.Damage == 3))
                    {
                        addIsOk = false;
                    }
                }

                foreach (var item in this.GameModel.GoldOnScreen)
                {
                    if (enemyToAdd.IsCollision(item))
                    {
                        addIsOk = false;
                    }
                }

                if (addIsOk)
                {
                    this.GameModel.EnemiesOnScreen.Add(enemyToAdd);
                }
            }

            // Food
            int foodToAddCount = 30 - this.GameModel.GoldOnScreen.Count;
            for (int i = 0; i < foodToAddCount; i++)
            {
                double xpos = (double)this.rng.Next(
                    (int)this.GameModel.GameWidth,
                    (int)this.GameModel.GameWidth * 2);
                double ypos = (double)this.rng.Next(150, (int)this.GameModel.GameHeight - 500);
                int value;
                bool randomapproach;
                if (this.rng.Next(0, 10) < 8)
                {
                    value = 1;
                    randomapproach = false;
                }
                else
                {
                    value = 3;
                    randomapproach = true;
                }

                double speed = 10;
                GoldModel foodToAdd = new GoldModel(xpos, ypos, value, randomapproach, speed);
                bool addIsOk = true;
                foreach (var item in this.GameModel.EnemiesOnScreen)
                {
                    if (foodToAdd.IsCollision(item))
                    {
                        addIsOk = false;
                    }
                }

                foreach (var item in this.GameModel.GoldOnScreen)
                {
                    if (foodToAdd.IsCollision(item))
                    {
                        addIsOk = false;
                    }
                }

                if (addIsOk)
                {
                    this.GameModel.GoldOnScreen.Add(foodToAdd);
                }
            }

            this.FillModelLogicPairs();
        }

        /// <inheritdoc/>
        public void IncreaseGoldCollected(int diff)
        {
            this.GameModel.GoldCollected += diff;
        }

        /// <inheritdoc/>
        public void StoreGoldCollected()
        {
            this.goldRepository.Add(this.GameModel.GoldCollected);
        }

        /// <inheritdoc/>
        public void IncreaseScore(int diff)
        {
            this.GameModel.Score += diff;
        }

        /// <inheritdoc/>
        public List<string> ListSavedMaps()
        {
            return this.mapRepository.ListSaves();
        }

        /// <inheritdoc/>
        public void LoadHighscore()
        {
            this.GameModel.Highscore = this.leaderboardRepository.GetHighscore();
        }

        /// <inheritdoc/>
        public void LoadMap(string savename)
        {
            this.GameModel.EnemiesOnScreen = this.mapRepository.Read(savename).EnemiesOnScreen;
            this.GameModel.GoldOnScreen = this.mapRepository.Read(savename).GoldOnScreen;
            this.GameModel.GoldCollected = this.mapRepository.Read(savename).GoldCollected;
            this.GameModel.Player.LivesTotal = this.mapRepository.Read(savename).Player.LivesTotal;
            this.GameModel.Player.LivesLeft = this.mapRepository.Read(savename).Player.LivesLeft;
            this.GameModel.Score = this.mapRepository.Read(savename).Score;
            this.GameModel.Level = this.mapRepository.Read(savename).Level;
            this.LoadHighscore();
            this.FlushModelLogicPairs();
            this.FillModelLogicPairs();
        }

        /// <inheritdoc/>
        public void SaveHighscore()
        {
            this.leaderboardRepository.Create(new LeaderboardEntryModel()
            {
                PlayerName = this.GetPlayerName(),
                Score = this.GameModel.Score,
            });
        }

        /// <inheritdoc/>
        public void SaveMap(string savename)
        {
            this.mapRepository.Create(this.GameModel, savename);
        }

        /// <inheritdoc/>
        public void SetHighscore(int highscore)
        {
            throw new NotImplementedException();
        }

        /// <inheritdoc/>
        public string GetPlayerName()
        {
            return PlayerModel.PlayerName;
        }

        /// <inheritdoc/>
        public void SetPlayerName(string playername)
        {
            PlayerModel.PlayerName = playername;
        }

        /// <summary>
        /// Resets all model logic pairs for a reload.
        /// </summary>
        private void FlushModelLogicPairs()
        {
            this.PlayerModelLogicPair = default;
            this.EnemyModelLogicPairs = new Dictionary<IEnemyModel, IEnemyLogic>();
            this.GoldModelLogicPairs = new Dictionary<IGoldModel, IGoldLogic>();
        }

        /// <summary>
        /// Create the necessary logics for all GameElements.
        /// </summary>
        private void FillModelLogicPairs()
        {
            if (this.PlayerModelLogicPair.Key == null)
            {
                this.PlayerModelLogicPair =
                    new KeyValuePair<IPlayerModel, IPlayerLogic>(
                        this.GameModel.Player,
                        new PlayerLogic(this.GameModel.Player));
            }
            else if (!this.PlayerModelLogicPair.Key.Equals(this.GameModel.Player))
            {
                this.PlayerModelLogicPair =
                    new KeyValuePair<IPlayerModel, IPlayerLogic>(
                        this.GameModel.Player,
                        new PlayerLogic(this.GameModel.Player));
            }

            foreach (var item in this.GameModel.EnemiesOnScreen)
            {
                if (!this.EnemyModelLogicPairs.ContainsKey(item))
                {
                    this.EnemyModelLogicPairs.Add(item, new EnemyLogic(item));
                }
            }

            foreach (var item in this.GameModel.GoldOnScreen)
            {
                if (!this.GoldModelLogicPairs.ContainsKey(item))
                {
                    this.GoldModelLogicPairs.Add(item, new GoldLogic(item));
                }
            }
        }
    }
}
