﻿// <copyright file="MenuLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Crossy.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Crossy.Logic.Interfaces;
    using Crossy.Model;
    using Crossy.Model.Interfaces;
    using Crossy.Repository;

    /// <summary>
    /// Menu logic class.
    /// </summary>
    public class MenuLogic : IMenuLogic
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="MenuLogic"/> class.
        /// </summary>
        /// <param name="gameModel">The model to manipulate.</param>
        public MenuLogic(IGameModel gameModel)
        {
            this.GameModel = gameModel ?? throw new ArgumentNullException(nameof(gameModel));
            this.LeaderboardRepository = new LeaderboardRepository();
            this.FishRepository = new GoldRepository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MenuLogic"/> class.
        /// </summary>
        /// <param name="gameModel">The model.</param>
        /// <param name="leaderboardRepository">The leaderboard repo.</param>
        /// <param name="fishRepository">The fish repo.</param>
        public MenuLogic(
            IGameModel gameModel,
            LeaderboardRepository leaderboardRepository,
            GoldRepository fishRepository)
        {
            this.GameModel = gameModel ?? throw new ArgumentNullException(nameof(gameModel));
            this.LeaderboardRepository = leaderboardRepository;
            this.FishRepository = fishRepository;
        }

        /// <inheritdoc/>
        public IGameModel GameModel { get; set; }

        private LeaderboardRepository LeaderboardRepository { get; }

        private GoldRepository FishRepository { get; }

        /// <inheritdoc/>
        public List<ILeaderboardEntryModel> DisplayHighscore()
        {
            return this.LeaderboardRepository.ReadAll().OrderByDescending(x => x.Score).ToList();
        }

        /// <inheritdoc/>
        public int GetCollectedGold()
        {
            return this.FishRepository.Read();
        }

        /// <inheritdoc/>
        public string GetPlayerName()
        {
            return PlayerModel.PlayerName;
        }

        /// <inheritdoc/>
        public void SetPlayerName(string playername)
        {
            PlayerModel.PlayerName = playername;
        }
    }
}
